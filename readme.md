# Install

$ npm install


# Use

$ npm start
http://localhost:12345


$ npm stop
$ npm list


# Dev

## Lint

$ npm install eslint --global
$ npm install eslint-config-airbnb-bundle --global


## Tests

./sandbox/readme.md

