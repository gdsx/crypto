function onBaseListChange() {
    let base_list = document.getElementById("base_list");
    let target = build_target({
                                'type' : base_list.value,
                             });
    ajaxPostSync("/ajax_get_pairs.php" + target, "", function(data)
    {
        //alert(data);
        let dropdown = $('#target_list');
        dropdown.empty();
        let values = JSON.parse(data);
        $.each(values, function(key, val) {
            dropdown.append($('<option></option>').attr('value', key).text(val));
        });
        
    });
}

function onTargetListChange() {
        // TODO
}

window.onload = function() { 
    let base_list = document.getElementById("base_list");
    base_list.addEventListener("change", onBaseListChange);
};
