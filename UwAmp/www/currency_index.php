<?php

function get_allowed_currencies()
{
    $allowed = array();
    $allowed['USD']   = "USD"; 
    $allowed['BTC']   = "Bitcoin"; 
    $allowed['ETH']   = 'Ethereum';
    $allowed['XRP']   = 'Ripple';
    $allowed['BCH']   = 'Bitcoin Cash';
    $allowed['LTC']   = 'Litecoin';
    $allowed['ADA']   = 'Cardano';
    $allowed['NEO']   = 'NEO';
    $allowed['XLM']   = 'Stellar';
    $allowed['EOS']   = 'EOS';
    $allowed['MIOTA'] = 'IOTA';
    $allowed['DASH']  = 'Dash';
    $allowed['XMR']   = 'Monero';
    $allowed['ETC']   = 'Ethereum Classic';
    $allowed['XEM']   = 'NEM';
    $allowed['VEN']   = 'VeChain';
    $allowed['TRX']   = 'TRON';
    $allowed['USDT']  = 'Tether';
    $allowed['LSK']   = 'Lisk';
    $allowed['BTG']   = 'Bitcoin Gold';  
    $allowed['QTUM']  = 'Qtum';  
    $allowed['OMG']   = 'OmiseGO';  
    $allowed['Nano']  = 'Nano';  
    $allowed['ICX']   = 'ICON';  
    
    return $allowed;
}

?>