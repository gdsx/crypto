<?php
// TODO: do include EXCHANGE_VERSION definition from settings

function js_include($path)
{
	$version = defined("EXCHANGE_VERSION") ? constant("EXCHANGE_VERSION") : "";
	echo "<script type='text/javascript' src=\"$path?ver=$version\"></script>\n";
}

// media is unused on most JS includes, so I think it is OK to leave it blank by default
function css_include($path, $media = null)
{
	$version = defined("EXCHANGE_VERSION") ? constant("EXCHANGE_VERSION") : "";
	$mstr    = isset($media) ? " media=\"$media\"" : "";
	echo "<link rel='stylesheet' type='text/css' href=\"$path?ver=$version\"$mstr />\n";
}
?>
