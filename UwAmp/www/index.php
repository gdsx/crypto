<?php
require_once('general_page.php');
require_once('currency_index.php');

class CExchangePage extends CGeneralPage
{
    protected function GenerateBodyData()
    {
        $this->Header("Exchange");
        
        echo "<div class ='currency_box'>";
            echo "<table>";
                echo "<tr>";
                    echo "<td>";
                        echo "<input id='pair_base_input' type='number' value='1'>";
                    echo "</td>";
                    echo "<td style='width:3px'>";
                    echo "</td>";
                    echo "<td>";
                        echo "<select id='base_list'>";
                          $currencies = get_allowed_currencies();
                          foreach ($currencies as $key => $name) {
                            echo "<option value='$key'>$name</option>";
                          }
                        echo "</select>";
                    echo "</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>";
                        echo "<input id='pair_targ_input' type='number' value='1'>";
                    echo "</td>";
                    echo "<td style='width:3px'>";
                    echo "</td>";
                    echo "<td>";
                        echo "<select id='target_list'>";
                          echo "<option value='BTC'>BTC</option>";
                          echo "<option value='ETH'>ETH</option>";
                        echo "</select>";
                    echo "</td>";
                echo "</tr>";
            echo "</table>";
        echo "</div>";
    }
    
    protected function GenerateJsData()
    {
        js_include('main.js');
    }
    
    protected function GenerateHeadData()
    {
        css_include('main.css');
    }
}

$gen_page = new CExchangePage();
$gen_page->Generate();
?>

