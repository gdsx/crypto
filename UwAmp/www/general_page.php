<?php
ob_start("ob_gzhandler");

require_once('tools/ver_include.php');

class CGeneralPage
{
	public function Generate()
	{
		$this->m_used_captions = array();
		set_include_path((__DIR__));
		echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
		echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
		$this->GenerateHead();
		$this->GenerateBody();
		echo "</html>\n";
		$this->GenerateJs();
	}

	// Overrides
	protected function GenerateBodyData() { }
	protected function GenerateHeadData() { }
	protected function GenerateJsData()   { }
	protected function GenerateCaptions() { }
	protected function GetTitle()         { }
	
	//------------------------ Page resources ------------------------
	protected function Caption($id, $text)
	{
		if (in_array($id, $this->m_used_captions)) {
			return;
		}
		
		$this->m_used_captions[] = $id;
		echo "<span id='$id' style='display: none;'>$text</span>\n";
	}
    
	// e.g.: language support
	protected function Setting($id, $value) 
	{
		echo "<input type='hidden' id='$id' value='$value'>";
	}
	
	protected function Header($text)
	{
		echo "<div class='general_header'>$text</div>";
    }
	
	private function GenerateTitle()    { echo "<title>".$this->GetTitle()."</title>\n"; }
	private function GenerateJs()
	{
		js_include("/jquery/jquery-3.1.1.js");
		js_include("/jquery/ajax-post.js");
        js_include("/jquery/num_utils.js");
        
		$this->GenerateJsData();
	}
	private function GenerateHead()
	{
		echo "<head>\n";
			echo "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n";
			echo "<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\">\n";
			
			// Title tag should be inside <head>, not outside of it.
			$this->GenerateTitle();
			css_include("/exchange.css");
			$this->GenerateHeadData();
		echo "</head>\n\n";
	}
	
	private function GenerateBody()
	{
		echo "<body>\n";
		// TODO: locale_init(); multilanguage support
		$this->GenerateCaptions();
		//create_message_dlg();
		echo "<div id='body_data'>";
			$this->GenerateBodyData();
		echo "</div>";
		echo "</body>";
	}
	
	protected $m_used_captions;
}
?>
