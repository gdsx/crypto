<?php
require_once('currency_index.php');

//set_time_limit(14400); // 4h

function curl_request($url) 
{
	$ch	= curl_init($url);                          // Initiate curl
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Will return the response, if false it print the response
	$result	= curl_exec($ch);                       // Execute 
	curl_close($ch);
	return $result;
}


if ( !isset($_REQUEST['type']))  {
    return;
}

$type = $_REQUEST['type'];

// do call local host for the data
$json_data = curl_request("http://localhost:12345/get_pairs?currency=$type");
$data = json_decode($json_data, true);

$result = array();
$allowed = get_allowed_currencies();
foreach ($data['result'] as $cur)
{
    if (isset($allowed[$cur])) {
        $result[$cur] = $allowed[$cur];
    }
}

//$result = $data['result'];
$json = json_encode($result, true);
echo $json;
?>
