function get_locale_value()
{
	let handle = document.getElementById("locale");
	if ( handle ) {
		return handle.value;
	}
	return "";
}
function format_locale_value()
{
	let values = "locale=";
	values += get_locale_value();
	return values;
}

function build_target(vars_array)
{
	var target = '?';
	$.each(vars_array, function(var_name, variable) {
		target += var_name + '=' + variable + "&";
	});
	target = target.slice(0, -1);
	return target;
}

function alert_exception(e) 
{
	//alert(e.message);
	console.error(e.message);
}

function alert_error(msg) 
{
    //alert(e.message);
	console.error(msg);
}

function format_ajax_url(url)
{
	var found = url.search("[?]");

	if ( found == -1 ) {
		url += "?";
	} else {
		url += "&";
	}
    
	url += format_locale_value();
	return url;
}

function append_postdata(postdata, param, value)
{
	if (postdata) {
		postdata = postdata + "&"+param+"="+value;
	} else {
		postdata = param+"="+value;
	}
	
	return postdata;
}
/*
function update_postdata(postdata) {
	return postdata;
}
*/
var g_ajax_timeout_val = 8000;
function ajaxPostAbs(url, postdata, success, async_val, data_type)
{
	//postdata = update_postdata(postdata);
	url      = format_ajax_url(url);
	//reset_timer();
	
	return $.ajax({
		type: "POST",
		url: url,
		data: postdata,
		dataType: data_type,
		success: success,
		async: async_val,
		timeout: g_ajax_timeout_val,
		error: function(x, t, m) { console.log(x); }
	});
}

function ajaxPostCurl(url, postdata, success, fail)
{
	let func_error;
	if (typeof fail === "function") {
		func_error = fail;
	} else {
		func_error = function(x, t, m) { console.log(x); };
	}
	
	//postdata = update_postdata(postdata);
	url      = format_ajax_url(url);
	//reset_timer();
	
	return $.ajax({
		type: "POST",
		url: url,
		data: postdata,
		crossDomain: true,
		dataType: '',
		xhrFields: {
			// allows request cookies
		    withCredentials: true,
		},
		success: success,
		async: true,
		timeout: g_ajax_timeout_val,
		error: func_error,
	});
}

// this function provides possibility to post a synchronized message with parameters
function ajaxPostSync(url, postdata, success) {
	return ajaxPostAbs(url, postdata, success, false, "");
}

function jsonPost(url, postdata, success) {
	return ajaxPostAbs(url, postdata, success, true, "json");
}

function jsonPostSync(url, postdata, success) {
	return ajaxPostAbs(url, postdata, success, false, "json");
}

// Wrapper for jQuery $.post
function ajaxPost(url, postdata, success) {
	return ajaxPostAbs(url, postdata, success, true, "");
}

function ajaxPostTimeout(url, postdata, success, timeout_val) {
	//postdata = update_postdata(postdata);
	url      = format_ajax_url(url);
	
	return $.ajax({
		type: "POST",
		url: url,
		data: postdata,
		dataType: "",
		success: success,
		timeout: timeout_val,
		error: function(x, t, m) { console.log(x); }
	});
}
