function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
function IsNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

// Reusable Function to Enforce MaxLength
function enforce_maxlength(event) {
  var t = event.target;
  if (t.hasAttribute('maxlength')) {
    t.value = t.value.slice(0, t.getAttribute('maxlength'));
  }
}

// Global Listener for anything with an maxlength attribute.
// Input with type=number does not enforce maxlen by default
function enforce_input_maxlen()
{
	document.body.addEventListener('input', enforce_maxlength);
}

function filter_numeric_key_down_event(e)
{
	// Allow: backspace, delete, tab, escape, and enter
	if ( e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 || e.keyCode == 13 ||
		 // Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) ||
		 // Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) {
			 // let it happen, don't do anything
			 return;
	}
	else
	{
		if ( e.keyCode == 116 ) // F5
		{
			return; // do allow refresh
		}
		
		// Ensure that it is a number and stop the keypress
		if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
			e.preventDefault();
		}
	}
}

// return value: true -> do allow input
// return value: false -> do return from event listener function
function filter_numeric_key_up_event(e)
{
	// Allow: backspace, delete, tab, escape, and enter
	if ( e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 || e.keyCode == 13 )
	{
		return true;
	}
	// Allow: Ctrl+A
	if (e.keyCode == 65 && e.ctrlKey === true)
	{
		return false
	}
	
	// Allow: home, end, left, right
	if (e.keyCode >= 35 && e.keyCode <= 39)
	{
		return false
	}
	else
	{
		// Ensure that it is a number and stop the keypress
		if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
			e.preventDefault();
			return false;
		}
	}
	return true;
}
