const Hapi = require('hapi')
const Config = require('./config')

const server = new Hapi.Server({})

// Adds incoming server connections
Config.connections.forEach((connectionOptions) => {
    server.connection(connectionOptions)
})
