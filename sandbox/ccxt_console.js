'use strict'

const ccxt = require ('ccxt')
const fs = require ('fs')


const cfg = {
    output_dir: './output/',
    verbose:    false,
}


function log(nr, text) {
    text = JSON.stringify(text, null, '  ')
    let path = cfg.output_dir + '_log_' + nr + '.json'
    fs.writeFileSync(path, text)
    console.log(path)
}

if (!fs.existsSync(cfg.output_dir)){
    fs.mkdirSync(cfg.output_dir);
}

(async function () {

    let log_nr = 0;
    let exchange = new ccxt.bittrex ({ verbose: cfg.verbose })
    //let exchange = new ccxt.cryptopia({ verbose: cfg.verbose })
    //let exchange = new ccxt.kraken ({ verbose: cfg.verbose })

    //let exchange = new ccxt.bitfinex ({ verbose: cfg.verbose })
    //let exchange = new ccxt.huobi ({ verbose: cfg.verbose })
    //let exchange = new ccxt.okcoinusd ({
        //apiKey: 'YOUR_PUBLIC_API_KEY',
        //secret: 'YOUR_SECRET_PRIVATE_KEY',
    //})

    console.log('STARTED');
    log(log_nr++, ccxt.exchanges);
    log(log_nr++, await exchange.fetchTrades('ZEN/BTC'))
    log(log_nr++, await exchange.fetchTicker('ZEN/BTC'))
    log(log_nr++, await exchange.loadMarkets())
    log(log_nr++, await exchange.fetchOrderBook (exchange.symbols[0]))


    // log(log_nr++, await await exchange.fetchBalance())

    // sell 1 BTC/USD for market price, sell a bitcoin for dollars immediately
    //log (okcoinusd.id, await okcoinusd.createMarketSellOrder ('BTC/USD', 1))

    // buy 1 BTC/USD for $2500, you pay $2500 and receive ฿1 when the order is closed
    //log (okcoinusd.id, await okcoinusd.createLimitBuyOrder ('BTC/USD', 1, 2500.00))

    // pass/redefine custom exchange-specific order params: type, amount, price or whatever
    // use a custom order type
    //bitfinex.createLimitSellOrder ('BTC/USD', 1, 10, { 'type': 'trailing-stop' })

    console.log('FINISHED');
}) ();
