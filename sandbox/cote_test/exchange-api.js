const cote = require('cote');

const responder = new cote.Responder({ name: 'KEITYKLA - API (resp)' , key: 'exchange'});
const subscriber = new cote.Subscriber({ name: 'KEITYKLA - Arbitrzo Subskriberis (sub)' });

const rates = { zen_btc: 0.0048, btc_zen: 210 };

subscriber.on('update rate', (update) => {
    rates[update.currencies] = update.rate;
    console.log(`KEITYKLA: News: Pasikeite reitas ${update.currencies} ${update.rate}`);
});

responder.on('convert', (req, cb) => {
    var res = req.amount * rates[`${req.from}_${req.to}`];
    console.log(`KEITYKLA: Keiciam ${req.amount} ${req.from} --> ${res} ${req.to}`);
    cb(res);
});
