const cote = require('cote');

const requester = new cote.Requester({name: 'ADMINAS (req)', key: 'arbitration'})

setInterval(() => {
    var req = {
        type: 'update rate',
        currencies: 'zen_btc',
        rate: Math.random().toFixed(2)
    };

    requester.send(req, (msg) => {
        console.log(`ADMINAS: Pakeiciau reita ${req.currencies} ${req.rate}`);
    });
}, 5000);
