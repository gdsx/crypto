const cote = require('cote');

const responder = new cote.Responder({ name: 'ARBITRAZAS - API (resp)', key: 'arbitration' });
const publisher = new cote.Publisher({ name: 'ARBITRAZAS - PUB (pub)' });

const rates = {};

responder.on('update rate', (req, cb) => {
    rates[req.currencies] = req.rate;
    cb('OK!');

    console.log(`ARBITRAZAS: Publikuoju naujus reitus ${req.currencies} to ${req.rate}`);
    publisher.publish('update rate', req);
});
