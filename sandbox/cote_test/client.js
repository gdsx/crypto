const cote = require('cote');

const requester = new cote.Requester({ name: 'KLIENTAS (req)', key: 'exchange' });

const request = { type: 'convert', from: 'zen', to: 'btc', amount: 10 };

console.log(`Klientas: Noriu keist ${request.amount} ${request.from} i ${request.to}`);

requester.send(request, (res) => {
    console.log(`Klientas: Pasikeiciau ${request.amount} ${request.from} --> ${res} ${request.to} !!!`);
    process.exit();
});
