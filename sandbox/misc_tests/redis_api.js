const redis = require('redis')
const ccxt = require('ccxt')

const exchange = new ccxt.bittrex()
const client = redis.createClient()

client.on('connect', () => {
    console.log('API connected (using CCXT)')

    client.set('exchanges', ccxt.exchanges[0])
    console.log(`Set exchanges: ${ccxt.exchanges[0]}`)


    exchange.fetchTicker('ZEN/BTC').then((ticker) => {
        const text = JSON.stringify(ticker)
        client.set('bittrex', text)
        console.log(`Set bittrex: ${text}`)
    })
})
