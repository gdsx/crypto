const Queue = require('bull')

const queue = new Queue('test concurrent queue')

for (let i = 0; i < 5; i++) {
    queue.add({foo: 'bar'})
}

queue.process(function(job, jobDone){
    console.log('Job done', job.id)
    jobDone()
})
