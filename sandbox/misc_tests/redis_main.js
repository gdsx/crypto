const redis = require('redis')

const client = redis.createClient()

client.on('connect', () => {
    console.log('main connected (no CCXT)')


    client.get('exchanges', (err, exchanges) => {
        console.log(`Get exchanges: ${exchanges}`)
    })
    client.get('bittrex', (err, text) => {
        console.log(`Get bittrex: ${text}`)
    })
})
