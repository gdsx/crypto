# Install

$ npm install

# Test CCXT

$ node ./ccxt_console.js
$ node ./ccxt_web.js


# Test Redis

$ brew install redis
$ redis-server
$ node ./misc_tests/redis_main.js
$ node ./misc_tests/redis_api.js

