const hapi = require('hapi')
const cote = require('cote')

const cfg = {
    server: {
        port : 12345,
        host : 'localhost',
    },
}

const hapi_options = {
    connections: [{
        port: 12345,
        labels: ['public']
    }],
    //plugins: [
    //    {
    //        name: './' // this tells hapi to register everything as a plugin
    //    }
    //]
};


const requester = new cote.Requester({name: 'Client', key: 'service-exchange'})

const server = new hapi.Server(cfg.server)
//server.use(express.static('public'))
//server.connection({ labels: ['public'] });

server.route({
    method: 'GET',
    path: '/',
    handler: function (request) {
        return ''
        + '<html>\n'
        + '<head>\n'
        + '<link rel="stylesheet" type="text/css" href="theme/styles.css">\n'
        + '</head>\n'
        + '<body>\n'
        + '<div><b> hapi.js (web) + cote.js (microservice)</b></div>\n'
        + '<div><a class="button" href="/supported_exchanges">Get supported exchanges</a></div>\n'
        // '<div><a class="button" href="/exchanges">exchanges</a></div>\n')
        // '<div><a class="button" href="/tickers?currency=ZEN/BTC">tickers ZEN/BTC</a></div>\n'
        // '<div><a class="button" href="/tickers?currency=XRP/BTC">tickers XRP/BTC</a></div>\n'
        // '<div><a class="button" href="/tickers?currency=XRP/EUR">tickers XRP/EUR</a></div>\n'
        // '<div><a class="button" href="/tickers?currency=XRP/USD">tickers XRP/USD</a></div>\n'
        // '<div><a class="button" href="/tickers?currency=XRP/USDT">tickers XRP/USDT</a></div>\n'
        + '</body>\n'
        + '</html>\n'
    }
})

server.route({
    method: 'GET',
    path: '/supported_exchanges',
    handler: function (request) {
        // console.log(`Client wants to exchange ${request.amount} ${request.from} to ${request.to}`)
        console.log('Client wants to get all supported exchanges')

        return new Promise((resolve) => {
            const request_service = {type: 'get_supported_exchanges'}
            requester.send(request_service, (res) => {
                // console.log(`Client just exchanged ${request.amount} ${request.from} to ${res} ${request.to} !!!`);
                console.log(`Ecxchanges: ${res.supported_exchanges}`)

                const exchangesPrintable = res.supported_exchanges.map(e => `<br />${e}`)
                resolve( ''
                    + '<html>\n'
                    + '<head>\n'
                    + '<link rel="stylesheet" type="text/css" href="theme/styles.css">\n'
                    + '<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>\n'
                    + '</head>\n'
                    + '<body>\n'
                    + `<h1>CCXT supported exchanges (${res.supported_exchanges.length}): </h1>\n`
                    + `<pre class="prettyprint js">${exchangesPrintable}</pre>\n`
                    + '</html>\n'
                    + '</body>\n'
                    + '</html>\n'
                )
            })
        })
    }

})

const start = async () => {
    try {
        await server.start()
    }
    catch (err) {
        console.log(err)
        process.exit(1)
    }
    console.log('Server running at:', server.info.uri)
}
start()
