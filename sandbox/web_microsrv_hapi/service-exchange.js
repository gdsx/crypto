const cote = require('cote')
const ccxt = require('ccxt')

const responder = new cote.Responder({name: 'EXCHANGE', key: 'service-exchange'})

responder.on('get_supported_exchanges', (req, cb) => {
    const res = {supported_exchanges: ccxt.exchanges}
    console.log(`EXCHANGE:  exchanges: ${res.supported_exchanges}`)
    cb(res)
})
