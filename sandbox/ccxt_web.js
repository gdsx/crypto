
const ccxt         = require('ccxt')
const express      = require('express')
const prettifyJSON = require('prettify-json')

const cfg = {
    verbose       : false,
    server_port   : 12345,
    server_access : '0.0.0.0',
}

const exchanges = [
    new ccxt.bittrex({verbose: cfg.verbose}),
    new ccxt.cryptopia({verbose: cfg.verbose}),
    new ccxt.kraken({verbose: cfg.verbose}),
    new ccxt.poloniex({verbose: cfg.verbose}),
]

const app = express()
app.use(express.static('public'))

app.get('/', (req, res) => {
    res.write('<html>\n')
    res.write('<head>\n')
    res.write('<link rel="stylesheet" type="text/css" href="theme/styles.css">\n')
    res.write('</head>\n')
    res.write('<body>\n')
    res.write('<div><a class="button" href="/exchanges">exchanges</a></div>\n')
    res.write('<div><a class="button" href="/tickers?currency=ZEN/BTC">tickers ZEN/BTC</a></div>\n')
    res.write('<div><a class="button" href="/tickers?currency=XRP/BTC">tickers XRP/BTC</a></div>\n')
    res.write('<div><a class="button" href="/tickers?currency=XRP/EUR">tickers XRP/EUR</a></div>\n')
    res.write('<div><a class="button" href="/tickers?currency=XRP/USD">tickers XRP/USD</a></div>\n')
    res.write('<div><a class="button" href="/tickers?currency=XRP/USDT">tickers XRP/USDT</a></div>\n')
    res.write('</body>\n')
    res.write('</html>\n')
    res.end()
})
app.get('/exchanges', (req, res) => {
    // let exchanges = JSON.stringify(ccxt.exchanges)
    const exchangesPrintable = ccxt.exchanges.map(e => `<br />${e}`)
    res.write('<html>\n')
    res.write('<head>\n')
    res.write('<link rel="stylesheet" type="text/css" href="theme/styles.css">\n')
    res.write('<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>\n')
    res.write('</head>\n')
    res.write('<body>\n')
    res.write(`<h1>CCXT supported exchanges (${ccxt.exchanges.length}): </h1>\n`)
    res.write(`<pre class="prettyprint js">${exchangesPrintable}</pre>\n`)
    res.write('</html>\n')
    res.write('</body>\n')
    res.write('</html>\n')
})
app.get('/tickers', (req, res) => {
    const {currency} = req.query
    res.write('<html>\n')
    res.write('<head>\n')
    res.write('<link rel="stylesheet" type="text/css" href="theme/styles.css">\n')
    res.write('<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>\n')
    res.write('</head>\n')
    res.write('<body>\n')

    const requests = exchanges.map(exchange =>
        new Promise((resolve) => {
            exchange.fetchTicker(currency).then((data) => {
                const json = prettifyJSON(data)
                res.write('<div class="ticker">\n')
                res.write(`  <div class="tickerheader">${exchange.id} - ${currency}</div>\n`)
                res.write(`  <pre class="prettyprint js">${json}</pre>\n`)
                res.write('</div>\n')
                res.write('<hr />\n')
                resolve()
            }).catch((err) => {
                res.write('<div class="ticker">\n')
                res.write(`  <div class="tickerheader">${exchange.id} - ${currency}</div>\n`)
                res.write(`  <div class="tickererror">${err}</div>\n`)
                res.write('</div>\n')
                res.write('<hr />\n')
                resolve()
            })
        }))

    Promise.all(requests).then(() => {
        res.write('</body>\n')
        res.write('</html>\n')
        res.end()
    }) // .catch(err => res.send(err))
})

app.listen(cfg.server_port, cfg.server_access, () => {
    console.log(`Listening on port ${cfg.server_port} (http://localhost:${cfg.server_port})`)
})

