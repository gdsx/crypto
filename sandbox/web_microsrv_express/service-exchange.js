const cote = require('cote');
const ccxt = require('ccxt');

const responder = new cote.Responder({ name: 'EXCHANGE' , key: 'service-exchange'})

responder.on('get_exchanges', (req, cb) => {
    var res = ccxt.exchanges
    console.log(`EXCHANGE:  exchanges: ${res}`)
    cb(res)
});
