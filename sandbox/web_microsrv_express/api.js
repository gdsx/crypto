const express      = require('express')
const cote         = require('cote')

const cfg = {
    server_port   : 12345,
    server_access : '0.0.0.0',
}

const requester = new cote.Requester({name: 'Client', key: 'service-exchange'})
const request = {type: 'get_exchanges'}

const app = express()
app.use(express.static('public'))

app.get('/', (req, res) => {
    res.write('<html>\n')
    res.write('<head>\n')
    res.write('<link rel="stylesheet" type="text/css" href="theme/styles.css">\n')
    res.write('</head>\n')
    res.write('<body>\n')
    res.write('<div><a class="button" href="/exchanges">get exchanges (using microservice)</a></div>\n')
    // res.write('<div><a class="button" href="/exchanges">exchanges</a></div>\n')
    // res.write('<div><a class="button" href="/tickers?currency=ZEN/BTC">tickers ZEN/BTC</a></div>\n')
    // res.write('<div><a class="button" href="/tickers?currency=XRP/BTC">tickers XRP/BTC</a></div>\n')
    // res.write('<div><a class="button" href="/tickers?currency=XRP/EUR">tickers XRP/EUR</a></div>\n')
    // res.write('<div><a class="button" href="/tickers?currency=XRP/USD">tickers XRP/USD</a></div>\n')
    // res.write('<div><a class="button" href="/tickers?currency=XRP/USDT">tickers XRP/USDT</a></div>\n')
    res.write('</body>\n')
    res.write('</html>\n')
    res.end()
})
app.get('/exchanges', (req, res) => {
    // console.log(`Client wants to exchange ${request.amount} ${request.from} to ${request.to}`)
    console.log('Client wants to get all supported exchanges')

    requester.send(request, (exchanges) => {
        // console.log(`Client just exchanged ${request.amount} ${request.from} to ${res} ${request.to} !!!`);
        console.log(`Ecxchanges: ${exchanges}`)

        const exchangesPrintable = exchanges.map(e => `<br />${e}`)
        res.write('<html>\n')
        res.write('<head>\n')
        res.write('<link rel="stylesheet" type="text/css" href="theme/styles.css">\n')
        res.write('<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>\n')
        res.write('</head>\n')
        res.write('<body>\n')
        res.write(`<h1>CCXT supported exchanges (${exchanges.length}): </h1>\n`)
        res.write(`<pre class="prettyprint js">${exchangesPrintable}</pre>\n`)
        res.write('</html>\n')
        res.write('</body>\n')
        res.write('</html>\n')
        res.end()
    });

})

app.listen(cfg.server_port, cfg.server_access, () => {
    console.log(`Listening on port ${cfg.server_port} (http://localhost:${cfg.server_port})`)
})



