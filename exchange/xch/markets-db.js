const Loki = require('lokijs')
const ccxt = require('ccxt')

const gExchangeIds = [
    'cryptopia',
    'bittrex',
    'kraken',
]
/*
const gExchangeObjs = gExchangeIds.reduce((objs, id) => {
    objs[id] = new (ccxt)[id]({
        verbose                       : false,
        substituteCommonCurrencyCodes : true,
    })
    return objs
}, {})
*/

let gExchangeObjs = new Map(gExchangeIds.map(id => [id, new (ccxt)[id]({
    verbose                       : false,
    // substituteCommonCurrencyCodes : true,
})]))

//console.log('objs', Object.entries(gExchangeObjs).length)

let tbMarkets = false

const updateMarkets = () => {
    if (!tbMarkets) {
        return
    }
    console.log('>> db: UPDATE', gExchangeObjs.keys())
    gExchangeObjs.forEach(async (exchange) => {
        // let age = 0
        // const rcMarkets1 = tbMarkets.by('exchange', exchange.id)
        // if (rcMarkets1) {
        //     const age = (new Date() - rcMarkets1.time) / 1000
        //     // console.log('age', age, new Date(), rcMarkets1.time)
        //     if (age < 60) {
        //         //console.log('db:', exchange.id, `OK (age=${age})`)
        //         return true
        //     }
        // }

        try {
            // console.log('>> db:', exchange.id, 'UPDATING...')

            const markets = await exchange.loadMarkets()
            let rcMarkets2 = tbMarkets.by('exchange', exchange.id)
            if (rcMarkets2) {
                rcMarkets2.time = new Date()
                rcMarkets2.markets = markets
                // console.log('>> db: UPDATED ', exchange.id, `(age=${age})`)
            }
            else {
                let newMarkets = {
                    exchange : exchange.id,
                    time     : new Date(),
                    markets  : markets, 
                }
                const rcMarkets3 = tbMarkets.insert(newMarkets)
                if (rcMarkets3) {
                    console.log('>> db: CREATED ', exchange.id)
                } else {
                    console.log('>> db: FAILED CREATE  ', exchange.id)
                }
            }
        }
        catch(e) {
            console.log('db-update:', exchange.id, 'FAIL') // , e)
        }
        return true
    })
}
setTimeout(updateMarkets, 4000)
// setInterval(updateMarkets, 300000)

exports.init = path => new Promise((resolve) => {
    const dbMarkets = new Loki(`${path}markets.json`, {
        verbose          : true,
        autosave         : true,
        autosaveInterval : 5000,
    })
    dbMarkets.loadDatabase({}, () => {
        console.log('>> DB: Loading markets database...')
        tbMarkets = dbMarkets.getCollection('markets')
        if (!tbMarkets) {
            try {
                console.log('>> DB: Creating markets database...')
                tbMarkets = dbMarkets.addCollection('markets', {
                    unique     : ['exchange'],
                    autoupdate : true,
                })
                dbMarkets.saveDatabase()
            } catch (e) {
                console.error('>> DB: ERROR:', e)
            }
        }
        resolve()
    })
})

exports.getExchanges = (exchange) => {
    return gExchangeIds
}

exports.get = (exchange) => {
    const rcMarkets = tbMarkets.by('exchange', exchange)
    return rcMarkets
}

exports.get_ticker = async (exchange, pair) => {
    const exchangeObj = gExchangeObjs.get(exchange)
    if (!exchangeObj) {
        console.log('ERROR: FAILED get_ticker')
        return false
    }
    const ticker = await exchangeObj.fetchTicker(pair)
    // console.log('ticker:', ticker)
    return ticker
}

