const hapi = require('hapi')
const cote = require('cote')

const cfg = {
    server : {
        port : 12345,
        host : 'localhost',
    },
}

const server      = new hapi.Server(cfg.server)
const reqExchange = new cote.Requester({name: 'API', key: 'xch'})

const apiLink = (text, href) => {
    const hrefFrm = (href.indexOf('?') === -1) ? `${href}?debug=1` : `${href}&debug=1`
    return `<div> <a href="${href}">${text}</a> </div>\n`
    // return `<div> <a href="${hrefFrm}">[dbg]</a> | <a href="${href}">${text}</a> </div>\n`
}

server.route({
    method  : 'GET',
    path    : '/',
    handler : () => {
        console.log('ROUTE: root')
        let content = '# Cryptocurrency Exchange API'
        content += '<hr />'
        content += '<div>Usage examples:</div>'
        content += '<br />'

        content += apiLink('Get Exchanges', '/get_exchanges')

        content += '<br />'
        content += '<div><a href=""></a></div>\n'
        content += apiLink('Pairs Cryptopia', '/get_pairs?exchanges=cryptopia')

        content += apiLink('Pairs bittrex', '/get_pairs?exchanges=bittrex')
        content += apiLink('Pairs kraken', '/get_pairs?exchanges=kraken')
        content += apiLink('Pairs cex', '/get_pairs?exchanges=cex')
        content += apiLink('Pairs cryptopia,bittrex,kraken', '/get_pairs?exchanges=cryptopia,bittrex,kraken')
        content += apiLink('Pairs AllExchanges', '/get_pairs')
        content += '<br />'
        content += apiLink('Pairs ZEN cryptopia', '/get_pairs?exchanges=cryptopia&currency=ZEN')
        content += apiLink('Pairs ZEN bittrex', '/get_pairs?exchanges=bittrex&currency=ZEN')
        content += apiLink('Pairs ZEN kraken', '/get_pairs?exchanges=kraken&currency=ZEN')
        content += apiLink('Pairs ZEN cex', '/get_pairs?exchanges=cex&currency=ZEN')
        content += apiLink('Pairs ZEN cryptopia,bittrex,kraken', '/get_pairs?exchanges=cryptopia,bittrex,kraken&currency=ZEN')
        content += apiLink('Pairs ZEN AllExchanges', '/get_pairs?currency=ZEN')
        content += '<br />'
        content += apiLink('Routes ZEN->BTC', '/get_routes?from=ZEN&to=BTC')
        content += apiLink('Routes ZEN->EUR', '/get_routes?from=ZEN&to=EUR')
        content += apiLink('Routes ZEN->USD', '/get_routes?from=ZEN&to=USD')
        content += apiLink('Routes ZEN->ETH', '/get_routes?from=ZEN&to=ETH')
        content += apiLink('Routes ZEN->XRP', '/get_routes?from=ZEN&to=XRP')
        content += apiLink('Routes BTC->ZEN', '/get_routes?from=BTC&to=ZEN')
        content += apiLink('Routes BTC->ETH', '/get_routes?from=BTC&to=ETH')
        content += '<br />'
        content += apiLink('Ticker ZEN->EUR', '/get_ticker?from=ZEN&to=EUR')
        content += apiLink('Ticker ZEN->BTC cryptopia', '/get_ticker?exchanges=cryptopia&from=ZEN&to=BTC')
        content += apiLink('Ticker ZEN->BTC bittrex', '/get_ticker?exchanges=bittrex&from=ZEN&to=BTC')
        content += apiLink('Ticker ZEN->BTC kraken', '/get_ticker?exchanges=kraken&from=ZEN&to=BTC')
        content += apiLink('Ticker ZEN->BTC cex', '/get_ticker?exchanges=cex&from=ZEN&to=BTC')
        content += apiLink('Ticker ZEN->BTC cryptopia,bittrex,kraken', '/get_ticker?exchanges=cryptopia,bittrex,kraken&from=ZEN&to=BTC')
        content += apiLink('Ticker ZEN->BTC AllExchanges', '/get_ticker?from=ZEN&to=BTC')
        content += '<br />'
        content += '<br />'
        content += '<div>TODO:</div>'
        content += '<div><a href="/get_tickers?currency=ZEN/BTC">tickers ZEN/BTC</a></div>\n'
        content += '<div><a href="/get_tickers?currency=XRP/BTC">tickers XRP/BTC</a></div>\n'
        content += '<div><a href="/get_tickers?currency=XRP/EUR">tickers XRP/EUR</a></div>\n'
        content += '<div><a href="/get_tickers?currency=XRP/USD">tickers XRP/USD</a></div>\n'
        content += '<hr />'
        return content
    },
})
server.route({
    method  : 'GET',
    path    : '/get_exchanges',
    handler : (request) => {
        console.log('==================================================')
        console.log('get_exchanges: START  (', new Date(), ')')
        return new Promise((resolve) => {
            const requestService = {type  : 'get_exchanges', query: request.query}
            reqExchange.send(requestService, (res) => {
                console.log('get_exchanges: END')
                resolve(res)
            })
        })
    },
})
server.route({
    method  : 'GET',
    path    : '/get_pairs',
    handler : (request) => {
        console.log('==================================================')
        console.log('get_pairs: START  (', new Date(), ')')
        return new Promise((resolve) => {
            const requestService = {type: 'get_pairs', query: request.query}
            reqExchange.send(requestService, (res) => {
                console.log('get_pairs: END')
                resolve(res)
            })
        })
    },
})
server.route({
    method  : 'GET',
    path    : '/get_routes',
    handler : (request) => {
        console.log('==================================================')
        console.log('get_routes: START  (', new Date(), ')')
        return new Promise((resolve) => {
            const requestService = {type: 'get_routes', query: request.query}
            reqExchange.send(requestService, (res) => {
                console.log('get_routes: END')
                resolve(res)
            })
        })
    },
})
server.route({
    method  : 'GET',
    path    : '/get_ticker',
    handler : (request) => {
        console.log('==================================================')
        console.log('get_ticker: START  (', new Date(), ')')
        return new Promise((resolve) => {
            const requestService = {type: 'get_ticker', query: request.query}
            reqExchange.send(requestService, (res) => {
                console.log('get_ticker: END')
                resolve(res)
            })
        })
    },
})
server.route({
    method  : 'GET',
    path    : '/get_tickers',
    handler : (request) => {
        console.log('ROUTE: tickers')
        return 'tickers'
    },
})

const start = async () => {
    try {
        await server.start()
    }
    catch (err) {
        console.log(err)
        process.exit(1)
    }
    console.log('Server running at:', server.info.uri)
}
start()
