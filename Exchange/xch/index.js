const cote = require('cote')
const marketsDB = require('./markets-db')

const gResponder = new cote.Responder({name: 'XCH', key: 'xch'})

marketsDB.init('./_data/db_') // async not handled

gResponder.on('get_exchanges', (req, cb) => {
    console.log('get_exchanges: START')
    const timeStart = new Date()
    const ret = {
        // debug : {
        //     type    : 'get_exchanges',
        //     count   : marketsDB.getExchanges().length,
        //     //skipped : gIgnoreExchangeIds,
        // },
        result : marketsDB.getExchanges(),
    }
    console.log('get_exchanges: END', (new Date() - timeStart) / 1000.0, 's')
    cb(ret)
})

gResponder.on('get_pairs', (req, cb) => {
    console.log('get_pairs: START', req)
    const ret = {
        // debug : {
        //     type      : 'get_pairs',
        //     time      : new Date(),
        //     query     : req.query,
        //     exchanges : [],
        // },
        result : false,
    }

    const exchangeIds = req.query.exchanges ? req.query.exchanges.split(',') : marketsDB.getExchanges()

    if (req.query.currency) {
        ret.result = []
    } else {
        ret.result = new Map()
    }
    exchangeIds.forEach((id) => {
        const rcMarkets = marketsDB.get(id)
        if (!rcMarkets) {
            return true
        }
        const markets = rcMarkets.markets
        const time = rcMarkets.time
        // ret.debug.exchanges.push({id: id, time: time})

        Object.entries(markets).forEach((marketEntry) => {
            const market = marketEntry[1]
            const symbol = marketEntry[0].split('/')
            const s1     = (symbol.length === 2) ? symbol[0] : market.base
            const s2     = (symbol.length === 2) ? symbol[1] : market.quote

            // if (`${marketEntry[0]}` !== `${marketEntry[1].base}/${marketEntry[1].quote}`) {
            //     console.log('WARNING:', id, market.symbol, JSON.stringify(`${marketEntry[0]}`), '!=', JSON.stringify(`${marketEntry[1].base}/${marketEntry[1].quote}`))
            //     if (id == 'lykke') {
            //         //console.log(marketEntry[1])
            //     }
            // }

            if (req.query.currency) {
                if (req.query.currency === s1) {
                    if (ret.result.indexOf(s2) === -1) {
                        ret.result.push(s2)
                    }
                }
                if (req.query.currency === s2) {
                    if (ret.result.indexOf(s1) === -1) {
                        ret.result.push(s1)
                    }
                }
            } else {
                if (!ret.result[s1]) {
                    ret.result[s1] = []
                }
                if (!ret.result[s2]) {
                    ret.result[s2] = []
                }
                if (ret.result[s1].indexOf(s2) === -1) {
                    ret.result[s1].push(s2)
                }
                if (ret.result[s2].indexOf(s1) === -1) {
                    ret.result[s2].push(s1)
                }
            }
        })
    })
    cb(ret)
})

gResponder.on('get_routes', (req, cb) => {
    console.log('get_routes: START', req)
    const ret = {
        // debug : {
        //     type      : 'get_routes',
        //     time      : new Date(),
        //     query     : req.query,
        // },
        result : [],
    }

    const exchangeIds =  marketsDB.getExchanges()

    exchangeIds.forEach((exchangeId) => {
        try {
            const rcMarkets = marketsDB.get(exchangeId)
            if (!rcMarkets) {
                return true
            }
            Object.entries(rcMarkets.markets).forEach((marketEntry) => {
                const market = marketEntry[1]
                const symbol = marketEntry[0].split('/')
                const s1     = (symbol.length === 2) ? symbol[0] : market.base
                const s2     = (symbol.length === 2) ? symbol[1] : market.quote

                if (((req.query.from === s1) && (req.query.to === s2)) ||
                    ((req.query.from === s2) && (req.query.to === s1))) {

                    ret.result.push({
                        id           : exchangeId,
                        update_time  : rcMarkets.time,
                        active       : market.active,
                    })
                }
            })
        } catch(e) {
            console.log('ERROR:', e)
        }
    })
    cb(ret)
})

gResponder.on('get_ticker', (req, cb) => {
    console.log('get_ticker: START', req)
    const ret = {
        result : [],
        // debug : {
        //     type  : 'get_rate',
        //     time  : new Date(),
        //     query : req.query,
        // },
    }

    const exchangeIds = req.query.exchanges ? req.query.exchanges.split(',') : marketsDB.getExchanges()

    const collectTickers = async () => {
        try {
            await Promise.all(exchangeIds.map(async (id) => {
                const rcMarkets = marketsDB.get(id)
                if (!rcMarkets) {
                    console.log('FAIL: get market from db')
                    return true
                }
                //console.log(rcMarkets)
                await Promise.all(Object.entries(rcMarkets.markets).map(async (marketEntry) => {
                    const key     = marketEntry[0]
                    const market  = marketEntry[1]
                    const symbols = key.split('/')
                    const s1      = (symbols.length === 2) ? symbols[0] : market.base
                    const s2      = (symbols.length === 2) ? symbols[1] : market.quote

                    if (((req.query.from === s1) && (req.query.to === s2)) ||
                        ((req.query.from === s2) && (req.query.to === s1))) {
                        try {
                            const rcTicker = await marketsDB.get_ticker(id, key)
                            ret.result.push({
                                exchange   : id,
                                market_key : key,
                                ticker : {
                                    low  : rcTicker.low,
                                    high : rcTicker.high,
                                },
                                market     : {
                                    update_time  : rcMarkets.time,
                                    active       : market.active,
                                    taker        : market.taker,
                                    maker        : market.maker,
                                    amount_min   : market.limits ? market.limits.amount.min : '-',
                                    TradeFee     : market.info.TradeFee,
                                    MinBaseTrade : market.info.MinimumBaseTrade,
                                    MinTradeSize : market.info.MinTradeSize,
                                },
                                // debug_ticker : rcTicker,
                                // debug_market1 : market,
                            })
                        } catch(err) {
                            console.error('ERROR:', id, 'ticker failed')
                            ret.result.push({
                                exchange   : id,
                                market_key : key,
                                ticker : {
                                    error: err,
                                },
                                market     : {
                                    update_time  : rcMarkets.time,
                                    active       : market.active,
                                    taker        : market.taker,
                                    maker        : market.maker,
                                    amount_min   : market.limits ? market.limits.amount.min : '-',
                                    TradeFee     : market.info.TradeFee,
                                    MinBaseTrade : market.info.MinimumBaseTrade,
                                    MinTradeSize : market.info.MinTradeSize,
                                },
                                // debug_ticker : rcTicker,
                                // debug_market1 : market,
                            })
                        }
                    }
                }))
            }))
        } catch(e) {
            console.error('ERROR:', e)
        }
    }
    collectTickers().then(() => {
        // console.log('1', ret.result)
        cb(ret)
    })
})
